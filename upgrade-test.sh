#!/bin/bash

set -euo pipefail

say() {
	echo "$@"
}

test_debian() {
	chronic python3 debian-test-suite.py --env DEBIAN_HOST=debian-test --env "HOME=$HOME" --log debian-test.log
}

say "Create VM with Debian stable (bullseye)"
chronic vmadm new debian-spec.yaml

say "Provision VM"
chronic ansible-playbook -i hosts playbook.yml

say "Ensure VM seems to work"
test_debian

say "Upgrade VM to Debian testing"
chronic ansible-playbook -i hosts upgrade.yml

say "Reboot VM"
chronic vmadm shutdown debian-spec.yaml
chronic vmadm start debian-spec.yaml

say "Ensure VM still seems to work"
test_debian

say "Delete VM"
chronic vmadm delete debian-spec.yaml

say "All good"
