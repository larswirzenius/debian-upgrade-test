# Debian upgrade test

This repository has a little script that does a simplistic Debian
upgrade test:

* create a virtual machine with the Debian stable release
* provision the VM with Ansible
  - this upgrades the VM to the current version of stable
  - the VM may be a little out of date
* run a test suite against the VM to verify it works
* upgrade the VM to run the current Debian testing branch that's going
  to become the next stable release
* reboot the VM
* re-run the test suite to verify it works

This uses my personal tooling, and may need porting to more generic
tooling for other people. Specifically, this uses my
[vmadm](https://vmadm.liw.fi/) tool to manage virtual machines. It
would not be too difficult to replace it with something else, even
direct use of `qemu-system`, but I'm not interested in that work.

## Notes

The current testing is very much simplistic. It's a proof of concept,
not a real test suite for Debian. However, it should be a good
starting point for something real.

To make this more realistic, change `playbook.yml` to install
a more representative system that you want to test. Maybe there should
be a desktop installed? Or a Postfix, PostgreSQL, and Apache setup?

The test suite is also very simplistic. See
[debian-subplot](https://gitlab.com/larswirzenius/debian-subplot) for
what it tests. That can be improved extensively, but current tests are
a minimal, but necessary start.

## Legalese

Copyright 2022 Lars Wirzenius <liw@liw.fi>

No warranty of any kind. Use at your own risk. Use under the MIT-0
license.
